#include <string>
#include "player.h"

player::player(std::string impName, std::string impTitle):
    name(impName), title(impTitle)
{

}

void player::setPlayerName(std::string newName){
    name = newName;
}
void player::setPlayerTitle(std::string newTitle){
    title = newTitle;
}
void player::setPlayerColor(Color newColor){
    color = newColor;
}
void player::setLifes(unsigned newLifesCount){
    lifes = newLifesCount;
}
void player::addScore(unsigned scoreCount){
    score = getScore() + scoreCount;
}