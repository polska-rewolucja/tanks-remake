#include "raylib.h"
#ifndef BASEUNIT_H
#define BASEUNIT_H

struct baseUnit
{
    virtual Vector2 getScreenPos(){ return screenPos; };
    virtual Vector2 getWorldPos(){ return worldPos; };
    void undoMovement();
    virtual void tick(float deltaTime);
    Rectangle getCollisionRec();
    bool getAlive(){ return stateAlive; };
    void setAlive(bool isAlive);

    protected:
    bool stateAlive{true};
    Texture2D texture;
    Texture2D moving;
    Texture2D idle;
    Vector2 screenPos{};
    Vector2 worldPos{0,0};
    Vector2 worldPosLast{0,0};
    float runningTime{};
    float updateTime{1/6};
    unsigned frame{};
    unsigned maxFrames{6};
    float scale{1.f};
    float speed{1.f};
    float width{};
    float height{};
    float rotate{};
    Vector2 direction{};
    Vector2 velocity{};
};

#endif