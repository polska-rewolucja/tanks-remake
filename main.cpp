#include "raylib.h"
#include "raymath.h"
#include <string>

#include "player.h"
#include "unit.h"

// Settings class
struct Settings{
    int fps{60};
    int windowWidth{800};
    int windowHeight{600};
    std::string gameName{"The game"};
};

int main()
{
    Settings global;
    SetTargetFPS(global.fps);
    InitWindow(global.windowWidth, global.windowHeight, global.gameName.c_str());

    player player1{"Player","Title"};
    unit tankPlayer1{true, 1, {12+16+256,12+16+256}, LoadTexture("gfx/units/player_idle.png"), LoadTexture("gfx/units/player_move.png")};

    while(!WindowShouldClose() && !IsKeyPressed(KEY_ESCAPE))
    {
        BeginDrawing();
        float deltaTime = GetFrameTime();

        // draw GUI
            DrawRectangle(0,0,global.windowWidth,global.windowHeight,GRAY);
            DrawRectangle(600,0,200,600,Color{100,100,100,255});

        // draw player statistics
            DrawText("Player 1: ", 610, 10, 20, BLACK);
            DrawText(player1.getPlayerName().c_str(), 710, 10, 20, BLACK);
            DrawText("Score: ", 610, 40, 20, BLACK);
            DrawText(std::to_string(player1.getScore()).c_str(), 685, 40, 20, MAROON);  

        // draw map
            Rectangle map{12 ,12 , 32 * 18, 32 * 18};
            DrawRectangleRec(map, BLACK);

        // tick functions
            tankPlayer1.tick(deltaTime);

        EndDrawing();
    }
}