#include <string>
#include "raylib.h"
#include "raymath.h"
#include "unit.h"

unit::unit(bool iPlayerType, unsigned iPlayerNumber, Vector2 iWorldPos, Texture iTextureIdle, Texture iTextureMove):
    playerType(iPlayerType), playerNumber(iPlayerNumber)
{
    worldPos = iWorldPos;
    texture = iTextureIdle;
    idle = iTextureIdle;
    moving = iTextureMove;    
    width = texture.width/maxFrames;
    height = texture.height;
}

void unit::tick(float deltaTime){
    // Save last position
    worldPosLast = worldPos;

    // Check if tile is reached
    if(static_cast<int>((worldPos.x-12)/1.6f) % 10 == 0 && static_cast<int>((worldPos.y-12)/1.6f) % 10 == 0)
    {
        // Reset velocity before action
        velocity = {};
        // Player control
        if(playerType){
            if(IsKeyDown(KEY_W)){
                velocity.y = -1.f;
                direction = velocity;
                rotate = 0.f;
            }else
            if(IsKeyDown(KEY_A)){
                velocity.x = -1.f;
                rotate = -90.f;
                direction = velocity;
            }else
            if(IsKeyDown(KEY_S)){
                velocity.y = 1.f;
                rotate = 180.f;
                direction = velocity;
            }else
            if(IsKeyDown(KEY_D)){
                velocity.x = 1.f;
                rotate = 90.f;
                direction = velocity;
            }
        }else if(!playerType){
            // AI control
        }
    }

    // Shoot a bullet
    if(playerType)
    {
        if(IsKeyPressed(KEY_SPACE)){
            // Create a bullet

            // Set delay time
        }
    }else if(!playerType){
        // Condition
        // Create a bullet

        // Set delay time
    }

    // Update position
    if(Vector2Length(velocity) != 0.0)
    {
        // Check boundaries of the map
        if( (velocity.x < 0 && worldPos.x-12-16 > 0)  ||
            (velocity.x > 0 && worldPos.x-12-16 < 544)||
            (velocity.y < 0 && worldPos.y-12-16 > 0)  ||
            (velocity.y > 0 && worldPos.y-12-16 < 544)){
                worldPos = Vector2Add(worldPos, Vector2Scale(Vector2Normalize(velocity), speed));
            }
        texture = moving;
    }else{
        texture = idle;
    }

    // Update animation
    runningTime += deltaTime;
    if(runningTime > updateTime)
    {
        frame++;
        runningTime = 0.f;
        if(frame > maxFrames) frame = 0;
    }

    // Draw unit
    Rectangle source{frame * width, 0.f, width, height};
    Rectangle dest{worldPos.x, worldPos.y ,width*scale, height*scale};
    DrawTexturePro(texture, source, dest,Vector2Scale({width/2, width/2},scale), rotate, WHITE);
}