#include "raylib.h"
#include "baseunit.h"

void baseUnit::undoMovement(){
    worldPos = worldPosLast;
}
Rectangle baseUnit::getCollisionRec(){
    return Rectangle{
        worldPos.x,
        worldPos.y,
        width * scale,
        height * scale
    };
}
void baseUnit::setAlive(bool isAlive){
    stateAlive = isAlive;
}
void baseUnit::tick(float deltaTime){

}