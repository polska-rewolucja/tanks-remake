#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include "raylib.h"

struct player
{
    player(std::string inpName, std::string impTitle);
    std::string getPlayerName() const { return name; };
    std::string getPlayerTitle() const { return title; };
    void setPlayerName(std::string newName);
    void setPlayerTitle(std::string newTitle);
    Color getPlayerColor() const { return color; };
    void setPlayerColor(Color newColor);
    unsigned getLifes() const { return lifes; };
    unsigned getScore() const { return score; };
    void setLifes(unsigned newLifesCount);
    void addScore(unsigned scoreCount);

    private:
    std::string name{"Player"};
    std::string title{"Czolgista"};
    Color color{200,0,0,255};
    unsigned lifes{3};
    unsigned score{};
};

#endif