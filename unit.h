#include "raylib.h"
#include "baseunit.h"

struct unit : public baseUnit
{
    unit(bool playerType, unsigned playerNumber, Vector2 iWorldPos, Texture iTextureIdle, Texture iTextureMove);
    virtual void tick(float deltaTime) override;
    
    private:
    // true - player : false - ai
    bool playerType{};
    // 0 - ai : 1, 2 - human players
    unsigned playerNumber{0};
    // unit type
    unsigned type{};
    float speed{2};
    unsigned hitPoints{1};
};